document.addEventListener("DOMContentLoaded", function () {
init();
});

function init(){
var video,player,mpd_url = "./stream.mpd";
video = document.querySelector("video");
player = dashjs.MediaPlayer().create();
player.initialize(video, mpd_url, true);
 var array_bitrate = {
  y: [0],
  x: [0],
  line: {
    color: 'rgb(255, 0, 0)',
    width: 2
  }
};
 var array_buffer = {
  y: [0],
  x: [0],
  line: {
    color: 'rgb(0, 0, 255)',
    width: 2
  }
};
var tiempo = 0
var titulo_bitrate = {
  title: 'Representacion Mbps vs tiempo'
};
var titulo_buffer = {
  title: 'Representacion  Buffer vs tiempo'
};
var PLOT_BITRATE = document.getElementById('plot_bitrate');
var PLOT_BUFFER = document.getElementById('plot_buffer');
Plotly.newPlot(PLOT_BITRATE, [array_bitrate], titulo_bitrate);
Plotly.newPlot(PLOT_BUFFER, [array_buffer],titulo_buffer);

player.on(dashjs.MediaPlayer.events["PLAYBACK_ENDED"], function() {clearInterval(eventPoller);
});

var eventPoller = setInterval(function() {
var streamInfo = player.getActiveStream().getStreamInfo();
var dashMetrics = player.getDashMetrics();
var dashAdapter = player.getDashAdapter();
if (dashMetrics && streamInfo) {
const periodIdx = streamInfo.index;
var repSwitch = dashMetrics.getCurrentRepresentationSwitch('video', true);
var bufferLevel = dashMetrics.getCurrentBufferLevel('video', true);
var bitrate = repSwitch ? Math.round(dashAdapter.getBandwidthForRepresentation(repSwitch.to,periodIdx) / 1000) : NaN;
tiempo= tiempo +0.5
array_buffer.y.push(bufferLevel);
array_buffer.x.push(tiempo);
array_bitrate.y.push(bitrate/1000);
array_bitrate.x.push(tiempo);
Plotly.newPlot(PLOT_BITRATE, [array_bitrate],titulo_bitrate);
Plotly.newPlot(PLOT_BUFFER, [array_buffer],titulo_buffer);
document.getElementById('buffer').innerText = bufferLevel + " secs";
document.getElementById('bitrate').innerText = bitrate + " Kbps";
document.getElementById('representation').innerText = repSwitch.to;
}
}, 500);
 
}
